# Translation of ejabberd debconf template to Swedish
# Copyright (C) 2024 Martin Bagge <brother@persilja.net>
# This file is distributed under the same license as the ejabberd package.
#
# Martin Bagge <brother@persilja.net>, 2008, 2015, 2024
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: ejabberd@packages.debian.org\n"
"POT-Creation-Date: 2017-11-21 23:34-0500\n"
"PO-Revision-Date: 2024-03-01 12:28+0100\n"
"Last-Translator: Martin Bagge <brother@persilja.net>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

# Note to translators:
# Please do not translate the variables ${hostname}, ${user}, ${preseed}, and
# any other which may appear in the future. Changes to these variables will
# break the scripts. Thank you!
#. Type: string
#. Description
#: ../templates:2001
msgid "Hostname for this Jabber server:"
msgstr "Värdnamn för Jabber-servern:"

#. Type: string
#. Description
#: ../templates:2001
msgid "Please enter a hostname for this Jabber server."
msgstr "Ange värdnamnet på Jabber-servern (endast gemener)."

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"If you would like to configure multiple hostnames for this server, you will "
"have to do so manually in /etc/ejabberd/ejabberd.yml after installation."
msgstr ""
"Om du vill ange flera värdnamn för denna server så behöver det göras "
"manuellt genom att redigera /etc/ejabberd/ejabberd.yml när paketet är "
"installerat."

#. Type: string
#. Description
#: ../templates:3001
msgid "Jabber server administrator username:"
msgstr "Användarnamn för jabber-serverns administratör:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Please provide the name of an account to administrate the ejabberd server. "
"After the installation of ejabberd, you can log in to this account using "
"either any Jabber client or a web browser pointed at the administrative "
"https://${hostname}:5280/admin/ interface."
msgstr ""
"Ange användarnamnet för ett administratörskonto för ejabberd-servern. Efter "
"att installationen är klar kan du använda detta konto för att logga in med "
"en Jabber-klient för att utföra administrativa uppgifter, alternativt så kan "
"du besöka https://${hostname}:5280/admin/ och logga in med kontot där för "
"att komma åt administrationsgränssnittet."

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"You only need to enter the username part here (such as ${user}), but the "
"full Jabber ID (such as ${user}@${hostname}) is required to access the "
"ejabberd web interface."
msgstr ""
"Du behöver endast ange användardelen här (exempelvis ${user}), komplett "
"Jabber-ID (ex. ${user}@${hostname}) krävs för att komma åt webbgränssnittet "
"för ejabberd."

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Please leave this field empty if you don't want to create an administrator "
"account automatically."
msgstr ""
"Lämna fältet tomt om du inte vill skapa ett administrationskonto automatiskt."

#. Type: password
#. Description
#: ../templates:4001
msgid "Jabber server administrator password:"
msgstr "Administrationslösenord för jabber-servern:"

#. Type: password
#. Description
#: ../templates:4001
msgid "Please enter the password for the administrative user."
msgstr "Ange lösenordet för administratörskontot."

#. Type: password
#. Description
#: ../templates:5001
msgid "Re-enter password to verify:"
msgstr "Upprepa för verifikation:"

#. Type: password
#. Description
#: ../templates:5001
msgid ""
"Please enter the same administrator password again to verify that you have "
"typed it correctly."
msgstr ""
"Ange administratörens lösenord en gång till för att bekräfta att det är "
"korrekt angivet."

#. Type: error
#. Description
#: ../templates:6001
msgid "Password input error"
msgstr "Fel vid inmatning av lösenord"

#. Type: error
#. Description
#: ../templates:6001
msgid ""
"The two passwords you entered did not match or were empty. Please try again."
msgstr "De båda lösenorden du angav är inte likadana. Vänligen försök igen."

#. Type: error
#. Description
#: ../templates:7001
msgid "Invalid administrator account username"
msgstr "Ogiltigt användarnamn för administrationskontot"

#. Type: error
#. Description
#: ../templates:7001
msgid ""
"The username previously specified contains forbidden characters. Please "
"respect the JID syntax (https://tools.ietf.org/html/rfc6122#appendix-A.5). "
"If you used a full JID (e.g. user@hostname), the hostname needs to match the "
"one previously specified."
msgstr ""
"Användarnamnet du angav innehåller otillåtna tecken. JID är standardiserat "
"(https://tools.ietf.org/html/rfc6122#appendix-A.5) och behöver efterlevas. "
"Om du använder ett komplett JID (ex. anv@example.com) måste värdnamn stämma "
"överens med det som angavs tidigare."

#. Type: error
#. Description
#: ../templates:8001
msgid "Invalid hostname"
msgstr "Ogiltigt värdnamn"

#. Type: error
#. Description
#: ../templates:8001
msgid ""
"The hostname previously specified contains forbidden characters or is "
"otherwise invalid. Please correct it and try again."
msgstr ""
"Värdnamnet som angavs innehåller förbjudna tecken eller är på annat sätt "
"ogiltigt. Justera det och försök igen."

#. Type: error
#. Description
#: ../templates:9001
msgid "Invalid preseeded configuration"
msgstr "Ogiltig förutbestämd inställning"

#. Type: error
#. Description
#: ../templates:9001
msgid ""
"A newer ${preseed} validation is being used and has determined that the "
"currently setup ${preseed} is invalid or incorrectly specified."
msgstr ""
"En nyare ${preseed} validering används och har bedömt att nuvarande "
"inställningar i ${preseed} är ogiltig eller felaktigt specificerade."

#. Type: error
#. Description
#: ../templates:9001
msgid ""
"If you would like to correct it, please backup your data and run dpkg-"
"reconfigure ejabberd after the upgrade is finished and note that any "
"databases and usernames will be lost or invalidated in this process if the "
"hostname is changed."
msgstr ""
"Om du vill justera detta gör först en säkerhetskopia av dina data och kör "
"sedan dpkg-reconfigure ejabberd efter uppgraderingen är slutförd. Databaser "
"och användarnamn kommer att förloras eller bli ogiltiga i och med denna "
"process om värdnamnet ändras."

#. Type: note
#. Description
#: ../templates:10001
msgid "Important changes to nodename (ERLANG_NODE) configuration"
msgstr "Viktiga ändringar för inställningar av nodename (ERLANG_NODE)"

#. Type: note
#. Description
#: ../templates:10001
msgid ""
"The nodename has changed to reflect ejabberd's upstream recommended nodename "
"configuration (ejabberd@localhost) which saves effort when moving XMPP "
"domains to a different machine."
msgstr ""
"Värdet för nodename har ändrats för att harmonisera med ejabberd-projektets "
"rekommendationer för inställningen nodename (ejabberd@localhost) vilket "
"minskar jobbet som behövs när en XMPP-domän flyttas till en ny maskin."

#. Type: note
#. Description
#: ../templates:10001
msgid ""
"This may break the current installation but may easily be fixed by editing "
"the ERLANG_NODE option in /etc/default/ejabberd either back to ejabberd or "
"to the name it was manually specified."
msgstr ""
"Detta kan trasa sönder nuvarande installation men kan enkelt lagas genom att "
"redigera värdet för ERLANG_NODE i /etc/default/ejabberd, antingen tillbaka "
"till ejabberd eller till namnet som tidigare var specificerat i dess ställe."

#. Type: note
#. Description
#: ../templates:10001
msgid ""
"Another way to fix a broken installation is to use ejabberdctl's "
"mnesia_change_nodename option to change the nodename in the mnesia database. "
"More information on this method may be found on the ejabberd guide (https://"
"docs.ejabberd.im/admin/guide/managing/#change-computer-hostname). Please "
"make appropriate backups of the database before attempting this method."
msgstr ""
"Ett annat sätt att laga en trasig installation är att använda alternativet "
"mnesia_change_nodename i ejabberdctl för att ändra nodename i mnesia-"
"databasen. Mer information om denna metod kan hittas i ejabberd-guiden "
"(https://docs.ejabberd.im/admin/guide/managing/#change-computer-hostname). "
"Vänligen gör säkerhetskopior på databasen innan du försöker dig på denna "
"metod."

# Note to translators:
# Please do not translate the variables ${hostname}, ${user}, ${preseed}, and
# any other which may appear in the future. Changes to these variables will
# break the scripts. Thank you!
#. Type: string
#. Description
#: ../templates:11001
msgid "ERL_OPTIONS for this ejabberd server:"
msgstr "ERL_OPTIONS för denna ejabberd-server::"

#. Type: string
#. Description
#: ../templates:11001
msgid ""
"To run the ejabberd server with customized Erlang options, enter them here. "
"It is also possible to set them by editing /etc/ejabberd/ejabberdctl.cfg. "
"See the erl(1) man page for more information."
msgstr ""
"Det går att köra ejabberd-server med anpassade Erlang-inställningar, ange "
"dessa här. Alternativt redigera filen /etc/ejabberd/ejabberdctl.cfg. och "
"ange de där. Läs även manual-sidan erl(1) för mer information."
